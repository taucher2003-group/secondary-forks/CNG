## FINAL IMAGE ##

ARG GITLAB_BASE_IMAGE=
ARG UBI_IMAGE=
ARG UBI_MICRO_IMAGE=

FROM ${UBI_MICRO_IMAGE} AS target


FROM ${UBI_IMAGE} AS build

ARG DNF_OPTS
ARG DNF_OPTS_ROOT
ARG GITLAB_USER=git
ARG UID=1000
ARG CONFIG_DIRECTORY=/etc/gitlab-pages
ARG DATA_DIRECTORY=/srv/gitlab-pages
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-pages.tar.gz ${DNF_INSTALL_ROOT}/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts

# Add shadow-utils to build and then adduser & create directories in ${DNF_INSTALL_ROOT} so they're available in final image
RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 shadow-utils \
    && adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/ \
    && mkdir -p ${DNF_INSTALL_ROOT}${CONFIG_DIRECTORY} ${DNF_INSTALL_ROOT}${DATA_DIRECTORY} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}${CONFIG_DIRECTORY} ${DNF_INSTALL_ROOT}${DATA_DIRECTORY} ${DNF_INSTALL_ROOT}/var/log/gitlab ${DNF_INSTALL_ROOT}/scripts \
    && chmod -R g=u ${DNF_INSTALL_ROOT}${CONFIG_DIRECTORY} ${DNF_INSTALL_ROOT}${DATA_DIRECTORY} ${DNF_INSTALL_ROOT}/var/log/gitlab ${DNF_INSTALL_ROOT}/scripts

# Add software to ${DNF_INSTALL_ROOT} so it's available in final image
RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0  procps libicu tzdata \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

FROM ${GITLAB_BASE_IMAGE}

ARG GITLAB_PAGES_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/gitlab-pages" \
      name="GitLab Pages" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_PAGES_VERSION} \
      release=${GITLAB_PAGES_VERSION} \
      summary="Serve static websites from GitLab repositories." \
      description="Serve static websites from GitLab repositories."

COPY --from=build  ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

ENV CONFIG_TEMPLATE_DIRECTORY=/etc/gitlab-pages

CMD ["/scripts/start-pages"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 \
CMD /scripts/healthcheck
