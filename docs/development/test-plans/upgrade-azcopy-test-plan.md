# `azcopy` component upgrade test plan

Steps to validate azcopy basic commands work:

1. Verify that the Pipeline is green.
1. Review the Changelog.
1. Login to your Azure account. If you don't have access, open an access request, [as per example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/34376).
1. Generate a Shared Access Signature (SAS) token for the Storage account. `cngazcopy` is the default Storage account for the test.

   - Go to Azure Portal.
   - Navigate to your Storage account, for example `cngazcopy`.
   - Click on "Shared access signature" in the left menu.
   - Select the permissions you need (typically just leave the default Read, Write, List, Delete)
   - Set an expiry time, or leave the default (1 day).
   - Click "Generate SAS and connection string".
   - Copy the SAS token and export in your terminal session:

      ```sh
      export AZURE_SAS_TOKEN="${YOUR_TOKEN_HERE}
      ```

1. Run the test script `./dev/azcopy/test.sh "renovate-azure-azure-storage-azcopy-10-x"`, with your docker engine running. The passed argument should match the image `TAG` built by your MR's pipeline. The script will test the tool version and the `list`, `copy` and `remove` commands, by creating, pushing, listing and removing a simple file called `1234567890_1234_12_12_12_TEST_gitlab_backup.tar`. You should see an output similar to:

   ```sh
   $ ./dev/azcopy/test.sh
   ...
   Checking The tool version:
   azcopy version 10.28.0

   Checking that the bucket is empty:
   {
   "TimeStamp": "2025-01-29T18:13:13.36677955Z",
   "MessageType": "EndOfJob",
   "MessageContent": "",
   "PromptDetails": {
      "PromptType": "",
      "ResponseOptions": null,
      "PromptTarget": ""
   }
   }
   copying file 1234567890_1234_12_12_12_TEST_gitlab_backup.tar to backup bucket https://cngazcopy.blob.core.windows.net/gitlab-backups/1234567890_1234_12_12_12_TEST_gitlab_backup.tar:

   Checking that the file got pushed to the bucket:
   {"TimeStamp":"2025-01-29T18:13:16.587689093Z","MessageType":"ListObject","MessageContent":"{\"Path\":\"1234567890_1234_12_12_12_TEST_gitlab_backup.tar\",\"ContentLength\":\"10.00 KiB\"}","PromptDetails":{"PromptType":"","ResponseOptions":null,"PromptTarget":""}}
   {"TimeStamp":"2025-01-29T18:13:16.589341593Z","MessageType":"EndOfJob","MessageContent":"","PromptDetails":{"PromptType":"","ResponseOptions":null,"PromptTarget":""}}

   Removing the file:

   Job ff66aefe-3cca-a544-4854-105152e00c24 has started
   Log file is located at: /home/git/.azcopy/ff66aefe-3cca-a544-4854-105152e00c24.log

   Job ff66aefe-3cca-a544-4854-105152e00c24 summary
   Elapsed Time (Minutes): 0.0336
   Number of File Transfers: 1
   Number of Folder Property Transfers: 0
   Number of Symlink Transfers: 0
   Total Number of Transfers: 1
   Number of File Transfers Completed: 1
   Number of Folder Transfers Completed: 0
   Number of File Transfers Failed: 0
   Number of Folder Transfers Failed: 0
   Number of File Transfers Skipped: 0
   Number of Folder Transfers Skipped: 0
   Total Number of Bytes Transferred: 10240
   Final Job Status: Completed

   Checking that the file got removed:
   {"TimeStamp":"2025-01-29T18:13:20.392892845Z","MessageType":"EndOfJob","MessageContent":"","PromptDetails":{"PromptType":"","ResponseOptions":null,"PromptTarget":""}}
   ```

 1. Verify your output to validate the version is what you expect, and that `list`, `copy`, and `remove` commands worked as expected.