ARG UBI_IMAGE=
ARG UBI_MICRO_IMAGE=

FROM ${UBI_MICRO_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS
ARG DNF_OPTS_ROOT

ARG TARGETARCH="amd64"
ARG TINI_VERSION=0.19.0

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target   / ${DNF_INSTALL_ROOT}/

# This will be "the" base image. Perform `microdnf upgrade` now.
RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} upgrade --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
    && microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
      findutils less procps curl ca-certificates tar tzdata shared-mime-info \
    && microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} reinstall --nodocs --best --assumeyes tzdata --setopt=install_weak_deps=0 \
    # remove soft-dependency on shadow-utils
    # See https://gitlab.com/gitlab-org/build/CNG/-/issues/2133
    && microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} remove --assumeyes shadow-utils \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

RUN microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
      curl-minimal ca-certificates tar \
    && microdnf clean all \
    && curl -L -o ${DNF_INSTALL_ROOT}/usr/bin/tini https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-${TARGETARCH} \
    && chmod +x ${DNF_INSTALL_ROOT}/usr/bin/tini

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts
RUN chgrp -R 0 ${DNF_INSTALL_ROOT}/scripts  && chmod -R g=u ${DNF_INSTALL_ROOT}/scripts

ADD gitlab-gomplate.tar.gz ${DNF_INSTALL_ROOT}/

FROM ${UBI_MICRO_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-base" \
      name="GitLab Base" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Base container for GitLab application containers." \
      description="Base container for GitLab application containers."

## Corrects CIS Server Level 1 benchmark finding, CCE-80783-4
## Details: http://static.open-scap.org/ssg-guides/ssg-rhel8-guide-standard.html#xccdf_org.ssgproject.content_rule_dir_perms_world_writable_sticky_bits
## RHEL Bug: https://bugzilla.redhat.com/show_bug.cgi?id=2138434
RUN chmod +t /tmp /var/tmp

COPY --from=build  ${DNF_INSTALL_ROOT}/ /

ENV FIPS_MODE=${FIPS_MODE}
## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

ENV CONFIG_TEMPLATE_DIRECTORY=/etc
ENV GITLAB_USER=${GITLAB_USER}

ENTRYPOINT ["/scripts/entrypoint.sh"]
