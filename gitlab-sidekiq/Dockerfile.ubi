## FINAL IMAGE ##
ARG RAILS_IMAGE=
ARG UBI_IMAGE=

FROM ${RAILS_IMAGE} AS target

FROM ${UBI_IMAGE} AS build

ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS
ARG DNF_OPTS_ROOT
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-sidekiq-ee.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-python.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-logger.tar.gz ${DNF_INSTALL_ROOT}/usr/local/bin/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts/

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 procps openssh-clients \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm ${DNF_INSTALL_ROOT}/usr/libexec/openssh/ssh-keysign \
    && mkdir -p ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && rm -f /install-root/var/lib/dnf/history*

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-sidekiq" \
      name="GitLab Sidekiq" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Sidekiq daemon." \
      description="Sidekiq daemon."

ENV SIDEKIQ_CONCURRENCY=25
ENV SIDEKIQ_TIMEOUT=25

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

# Declare /var/log volume after initial log files
# are written to the perms can be fixed
VOLUME /var/log

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
